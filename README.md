# Git in classroom

Git is a tool to manage repositories. A repository is a place where you have things, in our case documentation an exercises.

There is an origin repositori. For us this will be located at gitlab.com and it is created by the teacher. For example:

```
https://gitlab.com/cfgm-smx/m02
```

As you can see, that repository it can be accessed from a browser. Usually we will access a repository by cloning it on our machine.

## Cloning a repository
You will see the url for the git to be cloned on project website and it will be this format:

git@gitlab.com:cfgm-smx/m02.git

So we just clone it with git command, that should be installed on our system:

```
git clone git@gitlab.com:cfgm-smx/m02.git
```

This will create a folder named m02 with the contents of the project repository inside. As we did a copy of the project (cloned it) we can modify it's contents as we want.

But usually a project is managed by someone (your teacher) and won't allow by default to push (upload) your modifications. Instead, if we want to merge any modifications we have done we should send the manager a merge request. But we will talk about that later. First, let's see how we can create our own repository.

## Create our own repositories

### First create your account
There are many git cloud services available, but we will be using this gitlab.com, so go to the main page and register your user.

### Create new project
As soon as you got registered, you will be able to create a new project and also add files and folders and edit them. But we are not going to use the web interface, as we prefer to edit our local clone (copy).

You could keep your repository private or public, but in any case, to allow pushing local changes to your project repository origin (gitlab.com) you should authenticate your computer with your gitlab account. Let's do it!

### Authenticate your computer
For easy of use we can avoid using username and password when cloning/pushing our repository by using ssh keys.

SSH keys are encrypted public/private pair keys that should be shared (copied) from your computer to gitlab.com. This way gitlab will trust your computer as your valid source.

The steps are:
1) Create your id key on your computer account: ssh-keygen
2) Show and copy contents of the generated key: cat .ssh/id_rsa.pub
3) Paste that key on your gitlab.com website: Go to your settings (top right icon) and paste and save it on the SSH Keys page.

Here you have an example of this process:

```
darta@mini:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/darta/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/darta/.ssh/id_rsa.
Your public key has been saved in /home/darta/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:/1234Muqaw4iMEA11sdH54dAclbtwRdCk1AVoCyM7t4 darta@mini
The key's randomart image is:
+---[RSA 2048]----+
|   +o.           |
|      =oo        |
| . . ooo         |
| .     .=        |
|.       = .      |
|  .    . +       |
|   .  . o .      |
|= . ..   o       |
|.  ...    .      |
+----[SHA256]-----+
darta@mini:~$ 
darta@mini:~$ cat .ssh/id_rsa.pub 
ssh-rsa 1111B3NzaC1yc2EAAAADAQABAAABAQCztnPXD1lh0w0JBwxuQhm52Q3fuSa3p6MinJUbKYx3U1111CGeistxrHM6Ses8ydF3X7ovlUrdpI9VYNt8OaQgbfRlknDMaOt5kE589ca8gTGrgubMJZ/kjr+ViXudLBDTDiPl0xB+6MTgGgNccicliLgfPKos2rbb4ucNU501isaOOzIRs1NxaGaqT68+XSvzfHlu3Swy1Pr7wbs8rHJScQ8uVy3HbTJTwdJs7xNpM1AW24Y5taWEubBYQDzk9c/Ti+ey958GVzNN2JTatxkxdAMajNTVtdx4ZZGYKNoYekzN3p0S2cNBTSb4EyB6zOnNx/F2VBYrgDsiPAlO6WNp darta@mini
darta@mini:~$ git clone https://gitlab.com/cfgm-smx/m02.git
Clonant a 'm02'...
warning: Sembla que heu clonat un dipòsit buit.
Provant connectivitat... fet.
darta@mini:~$ 
```

So now we've got our repository cloned! Let's add some content and see how we can push it to the origin repository.

## Adding content

By default there should be a README.md file that will be shown as an introduction/resume of the project. It will be a file like this one (although it is usually not so large as this one).

So let's create that file:

```
vi README.md
```

That will open an editor to add content to README.md file. vi could be strange at first look but with the time you'll see how useful it can be, specially when you have to configure a simple operating system running in a network device. For example busybox on OpenWRT wireless router ;-)

To start editing press the 'i' key and to finish, save and quit press [ESC] + : + wq
That seem weird, but it is a minimal editor (but so powerful) that it is a must to know the basics of editing with it.

You can also install more 'user friendly' editor like nano by installing it:

Debian/Ubuntu:
apt install nano

Fedora/CentOS/RedHat
dns install nano

Then you could edit the file with:

```
nano README.md
```
